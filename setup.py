from os import path
from codecs import open
from setuptools import setup, find_packages


root = path.abspath(path.dirname(__file__))

with open(path.join(root, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='GraPy',
    version='0.1.0',
    description='Fast, memory-efficient graph data structures for Python!',
    long_description=long_description,
    url='https://bitbucket.org/kelechi_fletcher/grapy',
    author='Kelechi K. Fletcher',
    author_email='kelechi_fletcher@baylor.edu',
    license='GNU GPLv3',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'License :: OSI Approved :: GNU General Public License v3 or later '
        '(GPLv3+)',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='graphs graph-theory networks',
    packages=find_packages(exclude=['grapy.performance']),
    install_requires=['numpy', 'scipy'],
)

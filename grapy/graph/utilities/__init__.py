from collections import OrderedDict
from json import JSONEncoder
from operator import itemgetter
from grapy.core import Namespace
from grapy.graph.frame import NodeFrame, EdgeFrame

__all__ = ['nid', 'eid', 'adjacency', 'GraphEncoder', 'TreeEncoder']


_pair = itemgetter(0, 1)


# GRAPH UNIT ATTRIBUTION
nid = itemgetter(0)


def eid(edge):
    u, v = _pair(edge)
    return nid(u), nid(v)


adjacency = itemgetter(-1)


# JSON ENCODERS
class GraphEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, NodeFrame):
            return [OrderedDict(zip(r._fields[:-1], r)) for r in obj]

        elif isinstance(obj, EdgeFrame):
            return [OrderedDict(zip(r._fields, eid(r) + r[2:])) for r in obj]

        else:
            return JSONEncoder.default(self, obj)


class TreeEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Namespace):
            return obj.__dict__

        else:
            return JSONEncoder.default(self, obj)

from __future__ import division
from grapy.core.frame import Frame
from grapy.graph.units import *
from grapy.graph.view import *

__all__ = ['NodeFrame', 'IONodeFrame', 'EdgeFrame', 'DiEdgeFrame']


def _nodegetter(ntype=None):
    if not ntype:
        def get(obj, key):
            return obj[key]

    elif issubclass(ntype, BaseIONode):
        make_node = ntype.make

        def get(obj, key):
            return obj.setdefault(key, make_node((key, Frame({}), Frame({}))))

    else:
        make_node = ntype.make

        def get(obj, key):
            return obj.setdefault(key, make_node((key, Frame({}))))

    return get


class NodeFrame(Frame):
    __slots__ = ()

    @property
    def keys(self):
        return NodeKeyView(self._data)

    @property
    def values(self):
        return NodeValueView(self._data)

    @property
    def pairs(self):
        return NodePairView(self._data)

    def add(self, t, ntype=Node):
        """
        Add single node.

        :param t:
        :param ntype:
        :return:
        """

        if issubclass(ntype, BaseNode):
            data = self._data
            key = t[0]

            if key not in data:
                node = data[key] = ntype(*t, adj=Frame({}))

            else:
                raise KeyError('Node already associated'
                               ' with id {!r}'.format(key))

            return node

        else:
            raise AttributeError('No valid node type given.')

    def add_from(self, iterable, ntype=Node):
        """
        Add multiple nodes.

        :param iterable:
        :param ntype:
        :return:
        """

        if issubclass(ntype, BaseNode):
            data = self._data

            for t in iterable:
                key = t[0]

                if key not in data:
                    data[key] = ntype(*t, adj=Frame({}))

                else:
                    raise KeyError('Node already associated'
                                   ' with id {!r}'.format(key))

            return self

        else:
            raise AttributeError('No valid node type given.')

    def remove(self, key):
        """
        Remove single node.

        :param key:
        :return:
        """

        data = self._data
        node = data.pop(key)

        if node:
            for nbr_key in node.adj.keys:
                nbr = data[nbr_key]
                nbr.adj._data.pop(key)

        return node
    
    def remove_from(self, iterable):
        """
        Remove multiple nodes.

        :param iterable:
        :return:
        """

        data = self._data

        for key in iterable:
            node = data.pop(key)

            if node:
                for nbr_key in node.adj.keys:
                    nbr = data[nbr_key]
                    nbr.adj._data.pop(key)

        return self

    def replace(self, key, **kwargs):
        if 'id' not in kwargs:
            if 'adj' not in kwargs:
                data = self._data

                # replace node record
                node = data[key] = data[key].replace(**kwargs)

                # replace node references in adjacent edges
                for nbr_key, edge in node.adj.pairs:
                    nbr = data[nbr_key]
                    node.adj._data[nbr_key]\
                        = nbr.adj._data[key]\
                        = edge.replace(u=node, v=nbr)

                return node

            else:
                raise AttributeError("Cannot replace node attribute 'adj'.")

        else:
            raise AttributeError("Cannot replace node attribute 'id'.")


class IONodeFrame(NodeFrame):
    __slots__ = ()

    @property
    def keys(self):
        return IONodeKeyView(self._data)

    @property
    def values(self):
        return IONodeValueView(self._data)

    @property
    def pairs(self):
        return IONodePairView(self._data)

    def add(self, t, ntype=IONode):
        """
        Add I/O node record

        :param t:
        :param ntype:
        :return:
        """

        if issubclass(ntype, BaseIONode):
            data = self._data
            key = t[0]

            if key not in data:
                node = data[key] = ntype(*t, inn=Frame({}), out=Frame({}))

            else:
                raise KeyError('Node already associated'
                               ' with id {!r}'.format(key))

            return node

        else:
            raise AttributeError('No valid node type given.')

    def add_from(self, iterable, ntype=IONode):
        """
        Add I/O node records from iterable

        :param iterable:
        :param ntype:
        :return:
        """

        if issubclass(ntype, BaseIONode):
            data = self._data

            for t in iterable:
                key = t[0]

                if key not in data:
                    data[t[0]] = ntype(*t, inn=Frame({}), out=Frame({}))

                else:
                    raise KeyError('Node with already associated'
                                   ' with id {!r}'.format(key))

            return self

        else:
            raise AttributeError('No valid I/O node type given.')

    def remove(self, key):
        """
        Remove single I/O node.

        :param key:
        :return:
        """

        data = self._data
        src = data.pop(key)

        if src:
            for nbr_key in src.out.keys:
                dst = data[nbr_key]
                dst.inn._data.pop(key)

        return src

    def remove_from(self, iterable):
        """
        Remove multiple I/O nodes.

        :param iterable:
        :return:
        """

        data = self._data

        for key in iterable:
            src = data.pop(key)

            if src:
                for nbr_key in src.out.keys:
                    dst = data[nbr_key]
                    dst.inn._data.pop(key)

        return self

    def replace(self, key, **kwargs):
        if 'id' not in kwargs:
            if 'inn' not in kwargs:
                if 'out' not in kwargs:
                    data = self._data

                    # replace node record
                    data[key] = node = data[key].replace(**kwargs)

                    # replace node references in adjacent edges
                    for nbr_key, edge in node.out.pairs:
                        nbr = data[nbr_key]
                        node.out._data[nbr_key]\
                            = nbr.inn._data[key]\
                            = edge.replace(u=node, v=nbr)

                    return node

                else:
                    raise AttributeError("Cannot replace I/O "
                                         "node attribute 'out'.")

            else:
                raise AttributeError("Cannot replace I/O "
                                     "node attribute 'inn'.")

        else:
            raise AttributeError("Cannot replace I/O "
                                 "node attribute 'id'.")


class EdgeFrame(Frame):
    __slots__ = ()

    def __len__(self):
        return sum(len(vtx.adj) for vtx in self._data.values()) // 2

    def __iter__(self):
        seen = set()

        for u_key, node in self._data.items():
            for v_key, edge in node.adj.pairs:
                if v_key not in seen:
                    yield edge

            seen.add(u_key)

    def __contains__(self, key):
        data = self._data
        # unpack keys
        # NOTE: throws ValueError if len(key) != 2
        u_key, v_key = key

        return u_key in data and v_key in data[u_key].adj

    def __getitem__(self, key):
        # unpack keys
        # NOTE: throws ValueError if len(key) != 2
        u_key, v_key = key

        return self._data[u_key].adj[v_key]

    @property
    def keys(self):
        return EdgeKeyView(self._data)

    @property
    def values(self):
        return EdgeValueView(self._data)

    @property
    def pairs(self):
        return EdgePairView(self._data)

    def get(self, key, default=None):
        data = self._data
        # unpack keys
        # NOTE: throws ValueError if len(key) != 2
        u_key, v_key = key

        return data[u_key].get(key, default) if u_key in data else default

    def add(self, t, etype=Edge, fill_ntype=None):
        if issubclass(etype, BaseEdge):
            data = self._data
            get = _nodegetter(fill_ntype)

            # unpack keys
            u_key = t[0]
            v_key = t[1]

            # get incident nodes
            u = get(data, u_key)
            v = get(data, v_key)

            # add edge
            edge = u.adj._data[v_key]\
                 = v.adj._data[u_key]\
                 = etype.make((u, v) + t[2:])

            return edge

        else:
            raise AttributeError('No valid edge type given.')

    def add_from(self, iterable, etype=Edge, fill_ntype=None):
        """
        Add edge records from iterable

        :param iterable:
        :param etype:
        :param fill_ntype:
        :return:
        """

        if issubclass(etype, BaseEdge):
            data = self._data
            edge_make = etype.make
            get = _nodegetter(fill_ntype)

            for t in iterable:
                # unpack node keys
                u_key = t[0]
                v_key = t[1]

                # get incident nodes
                u = get(data, u_key)
                v = get(data, v_key)

                # update node adjacency
                u.adj._data[v_key]\
                    = v.adj._data[u_key]\
                    = edge_make((u, v) + t[2:])

            return self

        else:
            raise AttributeError('No valid edge type given.')

    def remove(self, key):
        data = self._data
        u_key, v_key = key
        data[u_key].adj._data.pop(v_key)
        edge = data[v_key].adj._data.pop(u_key)

        return edge

    def remove_from(self, iterable):
        data = self._data

        for u_key, v_key in iterable:
            data[u_key].adj._data.pop(v_key)
            data[v_key].adj._data.pop(u_key)

        return self

    def replace(self, key, **kwargs):
        if 'u' not in kwargs:
            if 'v' not in kwargs:
                data = self._data

                # unpack keys
                u_key, v_key = key

                # get incident node adjacency
                u = data[u_key]
                v = data[v_key]

                # update node adjacency with edge replacement
                edge = u.adj._data[v_key]\
                     = v.adj._data[u_key]\
                     = u.adj[v_key].replace(**kwargs)

                return edge

            else:
                raise AttributeError("Cannot replace edge attribute 'v'.")

        else:
            raise AttributeError("Cannot replace edge attribute 'u'.")


class DiEdgeFrame(EdgeFrame):
    __slots__ = ()

    def __len__(self):
        return sum(len(vtx.out) for vtx in self._data.values())

    def __iter__(self):
        for u_key, node in self._data.items():
            for v_key, edge in node.out.pairs:
                yield edge

    def __contains__(self, key):
        data = self._data
        # unpack keys
        # NOTE: throws ValueError if len(key) != 2
        u_key, v_key = key

        return u_key in data and v_key in data[u_key].out

    def __getitem__(self, key):
        # unpack keys
        # NOTE: throws ValueError if len(key) != 2
        u_key, v_key = key

        return self._data[u_key].out[v_key]

    @property
    def keys(self):
        return DiEdgeKeyView(self._data)

    @property
    def values(self):
        return DiEdgeValueView(self._data)

    @property
    def pairs(self):
        return DiEdgePairView(self._data)

    def add(self, t, etype=DiEdge, fill_ntype=None):
        """
        Add single directed edge.

        :param t:
        :param etype:
        :param fill_ntype:
        :return:
        """

        if issubclass(etype, BaseDiEdge):
            data = self._data
            get = _nodegetter(fill_ntype)

            # unpack node keys
            src_key = t[0]
            dst_key = t[1]

            # get incident nodes
            src = get(data, src_key)
            dst = get(data, dst_key)

            # initialize edge record
            edge = src.out._data[dst_key]\
                 = dst.inn._data[src_key]\
                 = etype.make((src, dst) + t[2:])

            return edge

        else:
            raise AttributeError('No valid directed edge type given.')

    def add_from(self, iterable, etype=DiEdge, fill_ntype=None):
        """
        Add multiple directed edges.

        :param iterable:
        :param etype:
        :param fill_ntype:
        :return:
        """

        if issubclass(etype, BaseDiEdge):
            data = self._data
            make_edge = etype.make
            get = _nodegetter(fill_ntype)

            for t in iterable:
                # unpack keys
                src_key = t[0]
                dst_key = t[1]

                # get incident nodes
                src = get(data, src_key)
                dst = get(data, dst_key)

                # update node adjacencies
                src.out._data[dst_key]\
                    = dst.inn._data[src_key]\
                    = make_edge((src, dst) + t[2:])

        else:
            raise AttributeError('No valid directed edge type given.')

    def remove_from(self, iterable):
        data = self._data

        for src_key, dst_key in iterable:
            del data[src_key].out._data[dst_key]
            del data[dst_key].inn._data[src_key]

    def replace(self, key, **kwargs):
        if 'src' not in kwargs:
            if 'dst' not in kwargs:
                data = self._data

                # unpack keys
                src_key, dst_key = key

                # get incident nodes
                src = data[src_key]
                dst = data[dst_key]

                # replace edge record
                edge = src.out._data[dst_key]\
                     = dst.inn._data[src_key]\
                     = src.out[dst_key].replace(**kwargs)

                return edge

            else:
                raise AttributeError("Cannot replace edge attribute 'dst'.")

        else:
            raise AttributeError("Cannot replace edge attribute 'src'.")

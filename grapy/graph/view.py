from grapy.core.view import KeyView, ValueView, PairView

__all__ = ['NodeKeyView', 'NodeValueView', 'NodePairView',
           'IONodeKeyView', 'IONodeValueView', 'IONodePairView',
           'EdgeKeyView', 'EdgeValueView', 'EdgePairView',
           'DiEdgeKeyView', 'DiEdgeValueView', 'DiEdgePairView']


class NodeKeyView(KeyView):
    pass


class NodeValueView(ValueView):
    pass


class NodePairView(PairView):
    pass


class IONodeKeyView(KeyView):
    pass


class IONodeValueView(ValueView):
    pass


class IONodePairView(PairView):
    pass


class EdgeKeyView(KeyView):
    def __iter__(self):
        for u_key, node in self._data.items():
            for v_key in node.adj.keys:
                yield (u_key, v_key)

    def __contains__(self, key):
        data = self._data
        # unpack keys
        # NOTE: throws ValueError if len(key) != 2
        u_key, v_key = key

        return u_key in data and v_key in data[u_key].adj


class EdgeValueView(ValueView):
    def __iter__(self):
        for node in self._data.values():
            for edge in node.adj.values:
                yield edge

    def __contains__(self, value):
        return any(value in node.adj.values for node in self._data.values())


class EdgePairView(PairView):
    def __iter__(self):
        for u_key, node in self._data.items():
            for v_key, edge in node.adj.pairs:
                yield (u_key, v_key), edge

    def __contains__(self, pair):
        return any(pair in node.adj.values for node in self._data.items())


class DiEdgeKeyView(KeyView):
    def __iter__(self):
        for u_key, node in self._data.items():
            for v_key in node.out.keys:
                yield (u_key, v_key)

    def __contains__(self, key):
        data = self._data
        # unpack keys
        # NOTE: throws ValueError if len(key) != 2
        u_key, v_key = key

        return u_key in data and v_key in data[u_key].out


class DiEdgeValueView(ValueView):
    def __iter__(self):
        for node in self._data.values():
            for edge in node.out.values:
                yield edge

    def __contains__(self, value):
        return any(value in node.out.values for node in self._data.values())


class DiEdgePairView(PairView):
    def __iter__(self):
        for u_key, node in self._data.items():
            for v_key, edge in node.out.pairs:
                yield (u_key, v_key), edge

    def __contains__(self, pair):
        return any(pair in node.out.values for node in self._data.items())

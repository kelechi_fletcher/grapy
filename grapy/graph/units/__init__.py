from grapy.graph.units.base import *
from grapy.graph.units.record import *

__all__ = ['Node', 'WeightedNode', 'IONode', 'WeightedIONode',
           'Edge', 'WeightedEdge', 'DiEdge', 'WeightedDiEdge',
           'BaseNode', 'BaseEdge', 'BaseIONode', 'BaseDiEdge',
           'TreeNode', 'node_record', 'edge_record']


# basic graph units
# node types
Node = node_record('Node')
WeightedNode = node_record('WeightedNode', ('weight', ))
IONode = node_record('IONode', is_io=True)
WeightedIONode = node_record('WeightedIONode', ('weight', ), is_io=True)

# edge types
Edge = edge_record('Edge')
WeightedEdge = edge_record('WeightedEdge', ('weight', ), defaults=(0.0,))
DiEdge = edge_record('DiEdge', is_directed=True)
WeightedDiEdge = edge_record('WeightedDiEdge', ('weight', ),
                             defaults=(0.0,), is_directed=True)

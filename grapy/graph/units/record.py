from collections import Iterable
from grapy.core.record import record
from grapy.graph.units.base import *

__all__ = ['node_record', 'edge_record']


def _node_record(typename, fields=(), defaults=(), bases=()):
    """
    Defines a node record class

    :param typename:
    :param fields:
    :param defaults:
    :param bases:
    :return:
    """

    # add required node fields and bases
    fields = ('id',) + tuple(map(str, fields)) + ('adj',)
    bases = (BaseNode,) + tuple(bases)

    return record(typename, fields, defaults, bases)


def _ionode_record(typename, fields=(), defaults=(), bases=()):
    """
    Defines a node record class w/ directed edges

    :param typename:
    :param fields:
    :param defaults:
    :param bases:
    :return:
    """

    # add required node fields and bases
    fields = ('id',) + tuple(map(str, fields)) + ('inn', 'out')
    bases = (BaseIONode,) + tuple(bases)

    return record(typename, fields, defaults, bases)


def _edge_record(typename, fields=(), defaults=(), bases=()):
    """
    Defines an edge record class

    :param typename:
    :param fields:
    :param defaults:
    :param bases:
    :return:
    """

    # add required edge fields and bases
    fields = ('u', 'v') + tuple(map(str, fields))
    bases = (BaseEdge,) + tuple(bases)

    return record(typename, fields, defaults, bases)


def _diedge_record(typename, fields=(), defaults=(), bases=()):
    """
    Defines a directed edge record class

    :param typename:
    :param fields:
    :param defaults:
    :param bases:
    :return:
    """

    # add required directed edge fields and bases
    fields = ('src', 'dst') + tuple(map(str, fields))
    bases = (BaseDiEdge,) + tuple(bases)

    return record(typename, fields, defaults, bases)


_NODE_FUNC_DICT = {False: _node_record, True: _ionode_record}
_EDGE_FUNC_DICT = {False: _edge_record, True: _diedge_record}


def node_record(typename, fields=(), defaults=(), bases=(), is_io=False):
    """
    Defines a node record class

    :param typename:
    :param fields:
    :param defaults:
    :param bases:
    :param is_io:
    :return:
    """

    return _NODE_FUNC_DICT[is_io](typename, fields, defaults, bases)


def edge_record(typename, fields=(), defaults=(), bases=(), is_directed=False):
    """
    Defines an edge record class

    :param typename:
    :param fields:
    :param defaults:
    :param bases:
    :param is_directed:
    :return:
    """

    return _EDGE_FUNC_DICT[is_directed](typename, fields, defaults, bases)

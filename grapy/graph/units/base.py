from grapy.core import Frame, Namespace

__all__ = ['BaseNode', 'BaseIONode', 'BaseEdge', 'BaseDiEdge', 'TreeNode']


class BaseNode(object):
    __slots__ = ()

    def __len__(self):
        raise NotImplementedError

    def __iter__(self):
        raise NotImplementedError

    def __getitem__(self, item):
        raise NotImplementedError

    @property
    def adj(self):
        raise NotImplementedError

    @property
    def degree(self):
        return len(self.adj)

    @classmethod
    def make(cls, iterable):
        return NotImplementedError

    def replace(self, **kwargs):
        return NotImplementedError


class BaseIONode(BaseNode):
    __slots__ = ()

    def __len__(self):
        raise NotImplementedError

    def __iter__(self):
        raise NotImplementedError

    def __getitem__(self, item):
        raise NotImplementedError

    @property
    def adj(self):
        data = self.inn._data.copy()
        data.update(self.out._data)
        return Frame(data)

    @property
    def inn(self):
        raise NotImplementedError

    @property
    def out(self):
        raise NotImplementedError

    @property
    def in_degree(self):
        return len(self.inn)

    @property
    def out_degree(self):
        return len(self.out)

    def replace(self, **kwargs):
        return NotImplementedError


class BaseEdge(object):
    __slots__ = ()

    def __len__(self):
        raise NotImplementedError

    def __iter__(self):
        raise NotImplementedError

    def __getitem__(self, item):
        raise NotImplementedError

    @property
    def nodes(self):
        u, v = self[:2]
        return Frame({u.id: u, v.id: v})

    @classmethod
    def make(cls, iterable):
        raise NotImplementedError

    def replace(self, **kwargs):
        return NotImplementedError


class BaseDiEdge(BaseEdge):
    __slots__ = ()

    def __len__(self):
        raise NotImplementedError

    def __iter__(self):
        raise NotImplementedError

    def __getitem__(self, item):
        raise NotImplementedError

    @property
    def src(self):
        raise NotImplementedError

    @property
    def dst(self):
        raise NotImplementedError

    @property
    def nodes(self):
        src, dst = self[:2]
        return Frame({src.id: src, dst.id: dst})

    @classmethod
    def make(cls, iterable):
        raise NotImplementedError

    def replace(self, **kwargs):
        return NotImplementedError


class TreeNode(Namespace):
    def __init__(self, iterable=None, **kwargs):
        super(TreeNode, self).__init__(iterable, **kwargs)
        self.children = []

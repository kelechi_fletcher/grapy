from collections import deque, defaultdict
from itertools import chain
from operator import attrgetter
from numpy import array, matrix
from scipy.sparse import coo_matrix
from grapy.core import *
from grapy.graph.units import *
from grapy.graph.frame import *

__all__ = ['BaseGraph', 'Graph', 'DiGraph']

BaseGraph = record('BaseGraph', ('nodes', 'edges'))


class Graph(BaseGraph):
    __module__ = '__main__'
    __slots__ = ()

    def __new__(cls, nodes=None, edges=None):
        nframe = nodes
        eframe = edges

        if not (isinstance(nodes, NodeFrame) and isinstance(edges, EdgeFrame)):
            data = {}
            nframe = NodeFrame(data)
            eframe = EdgeFrame(data)

            if nodes:
                nframe.add_from(*nodes)

            if edges:
                eframe.add_from(*edges)

        elif nframe._data is not eframe._data:
            raise AttributeError('Node and edge frames do'
                                 ' not reference same dict.')

        return BaseGraph.__new__(cls, nframe, eframe)

    # LOGICAL OPERATORS/FUNCTIONS
    def __add__(self, other):
        if isinstance(other, BaseGraph):
            # initialize graph data
            adj = defaultdict(dict)
            data = {}

            # add nodes (prefix id)
            for i, node in self.nodes.pairs:
                i = 'u_{}'.format(i)
                data[i] = node.replace(id=i, adj=Frame(adj[i]))

            for i, node in other.nodes.pairs:
                i = 'v_{}'.format(i)
                data[i] = node.replace(id=i, adj=Frame(adj[i]))

            # add edges
            for (i, j), edge in self.edges.pairs:
                i = 'u_{}'.format(i)
                j = 'u_{}'.format(j)
                adj[i][j] = adj[j][i] = edge.replace(u=data[i], v=data[j])

            for (i, j), edge in other.edges.pairs:
                i = 'v_{}'.format(i)
                j = 'v_{}'.format(j)
                adj[i][j] = adj[j][i] = edge.replace(u=data[i], v=data[j])

            # initialize node and edge frames
            nodes = NodeFrame(data)
            edges = EdgeFrame(data)

            return Graph(nodes, edges)

        else:
            raise TypeError('TypeError: unsupported operand type(s) for +:'
                            '{!r} and {!r}'.format(type(self), type(other)))

    def __or__(self, other):
        if isinstance(other, BaseGraph):
            # initialize graph data and add nodes
            adj = defaultdict(dict)
            data = {i: node.replace(adj=Frame(adj[i]))
                    for i, node
                    in chain(self.nodes.pairs,
                             other.nodes.pairs)}

            # add edges
            for (i, j), edge in chain(self.edges.pairs, other.edges.pairs):
                adj[i][j] = adj[j][i] = edge.replace(u=data[i], v=data[j])

            # initialize node and edge frames
            nodes = NodeFrame(data)
            edges = EdgeFrame(data)

            return Graph(nodes, edges)

        else:
            raise TypeError('TypeError: unsupported operand type(s) for |:'
                            '{!r} and {!r}'.format(type(self), type(other)))

    def add(self, *others):
        g = Graph()

        for other in others:
            g += other

        return g

    def union(self, *others):
        g = Graph()

        for other in others:
            g |= other

        return g

    def is_sparse(self):
        """
        Returns whether or not graph is sparse

        :return:
        """

        return (len(self.edges) / len(self.nodes) ** 2) < (1 / 2)

    # FORMAT CONVERSIONS
    def as_tree(self, root):
        """
        Returns tree

        :param root:
        :return:
        """

        nodes = self.nodes
        queue = deque()
        visited = set()

        # initialize tree nodes
        t_nodes = {key: TreeNode(zip(node._fields[:-1], node))
                   for key, node
                   in self.nodes.pairs}

        # initialize queue
        src = nodes[root]
        queue.append((root, iter(src.adj.keys)))
        visited.add(root)

        while queue:
            # peek front of queue
            src, keys = queue[0]

            try:
                # next key -> edge pair
                dst = next(keys)

                # if neighbor not already visited
                if dst not in visited:
                    # yield edge
                    t_nodes[src].children.append(t_nodes[dst])

                    # push neighbors onto queue
                    queue.append((dst, iter(nodes[dst].adj.keys)))
                    visited.add(dst)

            except StopIteration:
                queue.popleft()

        return t_nodes[root]

    def to_dict(self, weight=attrgetter('weight')):
        """
        Returns weighted adjacency dictionary

        :param weight:
        :return:
        """

        # initialize adjacency map
        adjacency = {u_key: {v_key: weight(edge)
                             for v_key, edge
                             in node.adj.pairs}
                     for u_key, node
                     in self.nodes.pairs}

        return adjacency

    def to_list(self, weight=attrgetter('weight'), default=0.0):
        """
        Returns weighted adjacency list

        :param weight:
        :param default:
        :return:
        """

        nodes = self.nodes

        # get list dimensions
        n = len(nodes)

        # initialize adjacency list
        adjacency = [[default] * n for _ in range(n)]

        # initialize node index
        index = {key: i for i, key in enumerate(nodes.keys)}

        # initialize visited set
        visited = set()

        # iterate through the unique edges
        for i, (u_key, node) in enumerate(nodes.pairs):
            for v_key, edge in node.adj.pairs:
                if v_key not in visited:
                    j = index[v_key]
                    w = weight(edge)
                    adjacency[i][j] = w
                    adjacency[j][i] = w

            visited.add(u_key)

        return adjacency

    def to_array(self, weight=attrgetter('weight'), dtype=float, default=0.0):
        """
        Returns weighted adjacency array (NumPy array)

        :param weight:
        :param dtype:
        :param default:
        :return:
        """

        return array(self.to_list(weight, default), dtype=dtype)

    def to_dense(self, weight=attrgetter('weight'), dtype=float, default=0.0):
        """
        Returns weighted, dense adjacency matrix (NumPy matrix)

        :param weight:
        :param dtype:
        :param default:
        :return:
        """

        return matrix(self.to_list(weight, default), dtype=dtype)

    def to_sparse(self, weight=attrgetter('weight'), dtype=float, default=0.0):
        """
        Returns weighted, sparse adjacency matrix (SciPy sparse matrix)

        :param weight:
        :param dtype:
        :param default:
        :return: sparse matrix (COOrdinate format)
        """

        return coo_matrix(self.to_list(weight, default), dtype=dtype)

    # CUSTOM CONSTRUCTORS
    @classmethod
    def from_edges(cls, iterable, etype=Edge, fill_ntype=Node):
        """
        Constructs a new graph from edge records

        :param iterable:
        :param etype:
        :param fill_ntype:
        :return:
        """

        graph = cls()
        graph.edges.add_from(iterable, etype, fill_ntype)

        return graph

    @classmethod
    def from_cursor(cls, nodes, edges, module=globals()):
        """
        Constructs a new graph from database cursor(s)

        :param nodes:
        :param edges:
        :param module:
        :return:
        """

        data = {}
        nframe = NodeFrame(data)
        eframe = EdgeFrame(data)

        if nodes:
            cursor, typename = nodes
            fields = tuple(c.name for c in cursor.description)[1:]
            ntype = module.setdefault(typename, node_record(typename, fields))
            nframe.add_from(cursor, ntype)

        if edges:
            cursor, typename = edges
            fields = tuple(c.name for c in cursor.description)[2:]
            etype = module.setdefault(typename, edge_record(typename, fields))
            eframe.add_from(cursor, etype)

        return cls(nframe, eframe)


class DiGraph(Graph):
    __module__ = '__main__'
    __slots__ = ()

    def __new__(cls, nodes=None, edges=None):
        nframe = nodes
        eframe = edges

        if not (isinstance(nodes, IONodeFrame)
                and isinstance(edges, DiEdgeFrame)):
            data = {}
            nframe = IONodeFrame(data)
            eframe = DiEdgeFrame(data)

            if nodes:
                nframe.add_from(*nodes)

            if edges:
                eframe.add_from(*edges)

        elif nframe._data is not eframe._data:
            raise AttributeError('Node and edge frames do'
                                 ' not reference same dict.')

        return BaseGraph.__new__(cls, nframe, eframe)

    # FORMAT CONVERSIONS
    def as_tree(self, root):
        """
        Returns tree

        :param root:
        :return:
        """

        nodes = self.nodes
        queue = deque()
        visited = set()

        # initialize tree nodes
        t_nodes = {key: TreeNode(zip(node._fields[:-2], node))
                   for key, node
                   in self.nodes.pairs}

        # initialize queue
        src = nodes[root]
        queue.append((root, iter(src.out.keys)))
        visited.add(root)

        while queue:
            # peek front of queue
            src, keys = queue[0]

            try:
                # next key -> edge pair
                dst = next(keys)

                # if neighbor not already visited
                if dst not in visited:
                    # yield edge
                    t_nodes[src].children.append(t_nodes[dst])

                    # push neighbors onto queue
                    queue.append((dst, iter(nodes[dst].out.keys)))
                    visited.add(dst)

            except StopIteration:
                queue.popleft()

        return t_nodes[root]

    def to_list(self, weight=attrgetter('weight'), default=0.0):
        """
        Returns weighted adjacency list

        :param weight:
        :param default:
        :return:
        """

        nodes = self.nodes

        # get list dimensions
        n = len(nodes)

        # initialize adjacency list
        adjacency = [[default] * n for _ in range(n)]

        # iterate through the unique edges
        index = {key: i for i, key in enumerate(nodes.keys)}

        for i, node in enumerate(nodes):
            row = adjacency[i]
            for key, edge in node.out.pairs:
                row[index[key]] = weight(edge)

        return adjacency

    # ALT CONSTRUCTORS
    @classmethod
    def from_edges(cls, iterable, etype=DiEdge, fill_ntype=IONode):
        """
        Constructs a new graph from edge records

        :param iterable:
        :param etype:
        :param fill_ntype:
        :return:
        """

        graph = cls()
        graph.edges.add_from(iterable, etype, fill_ntype)

        return graph

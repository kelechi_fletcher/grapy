import grapy as gp
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from random import random
from pympler.asizeof import asizeof


def enumerate_clique(v):
    # initialize edge lists
    nodes = [(i, dict(id=i)) for i in range(v)]
    edges = [(i, j) for i in range(v) for j in range(i + 1, v)]

    # initialize NetworkX graph
    n = nx.Graph()
    n.add_nodes_from(nodes)
    n.add_edges_from(edges)

    # initialize GraPy graph
    g = gp.Graph.from_edges(edges)

    return n, g


def enumerate_weighted_clique(v):
    # initialize edge lists
    nodes = [(i, dict(id=i)) for i in range(v)]
    edges = [(i, j, random()) for i in range(v) for j in range(i + 1, v)]

    # initialize NetworkX graph
    n = nx.Graph()
    n.add_nodes_from(nodes)
    n.add_weighted_edges_from(edges)

    # initialize GraPy graph
    g = gp.Graph.from_edges(edges, gp.WeightedEdge)

    return n, g


def clique_memory_test(a, b, inc):
    mb = 10 ** 6
    n_points = []
    g_points = []

    for v in range(a, b, inc):
        n, g = enumerate_weighted_clique(v)
        n_size = asizeof(n, limit=2000) / mb
        g_size = asizeof(g, limit=2000) / mb

        n_points.append((v, n_size))
        g_points.append((v, g_size))

    n_points = np.array(n_points)
    g_points = np.array(g_points)

    plt.figure()
    plt.title('Memory Usage of Weighted Cliques (NetworkX v. GraPy)')
    plt.xlabel('# of vertices')
    plt.ylabel('memory usage (MB)')

    nx_x, nx_y = n_points.T
    nx_plt, = plt.plot(nx_x, nx_y, label='NetworkX', color='#2ecc71', linestyle='--')

    gpy_x, gpy_y = g_points.T
    gpy_plt, = plt.plot(gpy_x, gpy_y, label='GraPy', color='#27ae60')

    plt.legend(handles=[nx_plt, gpy_plt])
    plt.show()

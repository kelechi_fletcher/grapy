from itertools import starmap

__all__ = ['Namespace']


_avr = '{}: {!r}'.format


class Namespace(object):
    __module__ = '__main__'

    def __init__(self, obj=None, **kwargs):
        if obj:
            self.__dict__.update(obj)
        else:
            self.__dict__.update(kwargs)

    def __dir__(self):
        return self.__dict__.keys()

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__,
                               ', '.join(starmap(_avr, self.__dict__.items())))

    def __setattr__(self, key, value):
        self.__dict__[key] = value

    def update(self, obj, **kwargs):
        """
        Updates the namespace object from dict/iterable.

        :param obj:
        :param kwargs:
        :return:
        """

        if obj:
            self.__dict__.update(obj)
        else:
            self.__dict__.update(kwargs)

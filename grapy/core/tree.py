from operator import itemgetter


_key = itemgetter(0)


class Tree(object):
    __slots__ = ('_data', )

    def __init__(self):
        self._data = [None, ]

    def add(self, obj):
        data = self._data

        i = 0
        key = _key(obj)
        curr = _key(data[i])

        while curr:
            if key < curr:
                i = (2 * i) + 1

            elif key > curr:
                i = (2 * i) + 2

            else:
                break

            if not i < len(data):
                data.extend([None, None])

            curr = data[i]

        data[i] = obj

from collections import Iterable, OrderedDict
from itertools import starmap
from operator import itemgetter

__all__ = ['record']

_AVR_TEMPLATE = '{1}: {{{0}!r}}'

_NEW_FUNC_TEMPLATE = '''\
def __new__(cls, {0}):
    return tuple.__new__(cls, ({0},))
'''

_REPR_FUNC_TEMPLATE = '''\
def __repr__(self):
    return "{{}}({{}})".format(self.__class__.__name__, "{0}".format(*self))
'''

_STR_FUNC_TEMPLATE = '''\
def __str__(self):
    return "[{{}}]\\n{{}}".format(self.__class__.__name__, "{0}".format(*self))
'''

_avr = _AVR_TEMPLATE.format


def _new_func(fields, defaults):
    namespace = {}
    exec(_NEW_FUNC_TEMPLATE.format(', '.join(fields)), namespace)
    namespace['__new__'].__defaults__ = defaults

    return namespace['__new__']


def _repr_func(fields):
    namespace = {}
    exec(_REPR_FUNC_TEMPLATE
         .format(', '.join(starmap(_avr, enumerate(fields)))),
         namespace)

    return namespace['__repr__']


def _str_func(fields):
    namespace = {}
    exec(_STR_FUNC_TEMPLATE
         .format('\\n'.join(starmap(_avr, enumerate(fields)))),
         namespace)

    return namespace['__str__']


def _getnewargs_func(self):
    return tuple(self)


def _as_dict_func(self):
    return OrderedDict(zip(self._fields, self))


def _make_func(cls, iterable):
    return tuple.__new__(cls, iterable)


def _replace_func(self, **kwargs):
    return self.make(map(kwargs.pop, self._fields, self))


def record(typename, fields, defaults=(), bases=(), module='__main__'):
    """
    Defines a tuple-based class with named fields

    :param typename:
    :param fields:
    :param defaults:
    :param bases:
    :param module:
    :return:
    """

    # define fields
    fields = tuple(map(str, fields))

    # define base classes
    bases = (tuple,) + tuple(bases)

    # initialize type dict
    typedict = {'__slots__': (),
                '__new__': _new_func(fields, defaults),
                '__repr__': _repr_func(fields),
                '__str__': _str_func(fields),
                '__getnewargs__': _getnewargs_func,
                '_fields': fields,
                'as_dict': _as_dict_func,
                'make': classmethod(_make_func),
                'replace': _replace_func}

    # define field aliases
    for i, field in enumerate(fields):
        typedict[field] = property(itemgetter(i))

    # construct record type
    rtype = type(typename, bases, typedict)

    # define record type module
    rtype.__module__ = module

    return rtype

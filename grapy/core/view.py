__all__ = ['BaseView', 'KeyView', 'ValueView', 'PairView']


class BaseView(object):
    __slots__ = ('_data',)

    def __init__(self, data):
        self._data = data

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        raise NotImplementedError

    def __contains__(self, item):
        raise NotImplementedError


class KeyView(BaseView):
    def __iter__(self):
        return iter(self._data)

    def __contains__(self, key):
        return key in self._data

    def __repr__(self):
        if len(self) <= 5:
            keys = ', '.join(map(repr, self))

        else:
            it = iter(self)
            keys = ', '.join(map(repr,
                                 (next(it) for _ in range(5)))) + ', ... '

        return '{}({{{}}})'.format(self.__class__.__name__, keys)


class ValueView(BaseView):
    def __iter__(self):
        return iter(self._data.values())

    def __contains__(self, value):
        return value in self._data.values()

    def __repr__(self):
        if len(self) <= 5:
            values = ', '.join(map(repr, self))

        else:
            it = iter(self)
            values = ', '.join(map(repr,
                                   (next(it) for _ in range(5)))) + ', ... '

        return '{}({{{}}})'.format(self.__class__.__name__, values)


class PairView(BaseView):
    def __iter__(self):
        return iter(self._data.items())

    def __contains__(self, pair):
        return pair in self._data.items()

    def __repr__(self):
        if len(self) <= 5:
            pairs = ', '.join(map(repr, self))

        else:
            it = iter(self)
            pairs = ', '.join(map(repr,
                                  (next(it) for _ in range(5)))) + ', ... '

        return '{}({{{}}})'.format(self.__class__.__name__, pairs)

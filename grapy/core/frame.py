from grapy.core.view import KeyView, ValueView, PairView

__all__ = ['Frame']


class Frame(object):
    __slots__ = ('_data',)

    def __init__(self, data):
        self._data = data

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        return iter(self._data.values())

    def __contains__(self, key):
        return key in self._data

    def __getitem__(self, key):
        return self._data[key]

    def __getstate__(self):
        return {'_data': self._data}

    def __setstate__(self, state):
        self._data = state.get('_data')

    def __repr__(self):
        if len(self) <= 5:
            keys_repr = ', '.join(map(repr, self.keys))

        else:
            it = iter(self.keys)
            keys = (next(it) for _ in range(5))
            keys_repr = '{}, ...'.format(', '.join(map(repr, keys)))

        return '{0}({{{1}}})'.format(self.__class__.__name__, keys_repr)

    @property
    def keys(self):
        """
        Returns key view of frame

        :return: KeyView
        """

        return KeyView(self._data)

    @property
    def values(self):
        """
        Returns value view of frame

        :return: ValueView
        """

        return ValueView(self._data)

    @property
    def pairs(self):
        """
        Returns pair view of frame

        :return:
        """

        return PairView(self._data)

    @property
    def index_dict(self):
        return {key: i for i, key in enumerate(self._data)}

    def get(self, key, default=None):
        """
        Returns value corresponding to given key.
        If no corresponding value exists, returns default.

        :param key:
        :param default:
        :return:
        """

        return self._data.get(key, default)

from operator import attrgetter
from scipy.sparse import csgraph
from grapy.graph import *


def _to_minimum_spanning_tree(obj, dist, pred, sources, index=None):
    trees = {}

    if not index:
        for n, src in enumerate(sources):
            # generate nodes from distances
            gennodes = ((i, dist)
                        for i, dist
                        in enumerate(dist[n]))

            # generate edges from predecessors
            genedges = ((u, v, obj[u, v])
                        for v, u
                        in enumerate(pred[n])
                        if u >= 0)

            trees[src] = DiGraph(nodes=(gennodes, WeightedIONode),
                                 edges=(genedges, WeightedDiEdge))

    else:
        key = [None] * len(index)

        for k, v in index.items():
            key[v] = k

        for n, src in enumerate(sources):
            # generate nodes from distances
            gennodes = ((key[i], dist)
                        for i, dist
                        in enumerate(dist[n]))

            # generate edges from predecessors
            genedges = ((key[u], key[v], obj[u, v])
                        for v, u
                        in enumerate(pred[n])
                        if u >= 0)

            trees[key[src]] = DiGraph(nodes=(gennodes, WeightedIONode),
                                      edges=(genedges, WeightedDiEdge))

    return trees


def dijkstra(obj, *sources, **kwargs):
    weight = kwargs.pop('weight', attrgetter('weight'))
    directed = kwargs.pop('directed', True)

    if isinstance(obj, BaseGraph):
        index = obj.nodes.index_dict
        sources = tuple(index[src] for src in sources)

        if obj.is_sparse():
            obj = obj.to_sparse(weight).tocsr()
        else:
            obj = obj.to_dense(weight)

    else:
        index = None

    dist, pred = csgraph.dijkstra(obj,
                                  indices=sources,
                                  directed=directed,
                                  return_predecessors=True)

    return _to_minimum_spanning_tree(obj, dist, pred, sources, index)

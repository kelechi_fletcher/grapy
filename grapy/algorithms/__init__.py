import numpy as np
from grapy.algorithms.paths import *
from grapy.algorithms.traverse import *

__all__ = ['pagerank', 'bfs', 'dfs', 'dijkstra']


def pagerank(graph, weight='weight', seed=None, alpha=0.85, err=1e-6):
    """
    Computes pagerank via approximation by power iteration

    :param graph:
    :param weight:
    :param seed:
    :param alpha:
    :param err:
    :return:
    """

    # convert graph to probability matrix
    arr = graph.to_array(weight, float)
    arr /= arr.sum(axis=1)

    # initialize PageRank vector
    if not seed:
        n, _ = arr.shape
        rank = np.repeat(1 / n, n)

    else:
        seed = set(seed)
        p = 1 / len(seed)
        p_iter = (p for n_id in graph.nodes.keys if n_id in seed)
        rank = np.fromiter(p_iter, float)

    # initialize google matrix
    g_arr = alpha * arr + (1 - alpha) * rank

    # power iteration for approximating PageRank
    converged = False

    while not converged:
        prox = rank.copy()
        rank = g_arr.dot(rank)

        if np.linalg.norm(rank - prox) <= err:
            converged = True

    return dict(zip(graph.nodes.keys, rank))

from collections import deque
from grapy.graph.graph import Graph, DiGraph
from grapy.graph.utilities import adjacency


def _replace_edge(u, v, edge):
    return edge.replace(u=u, v=v)


def _replace_diedge(src, dst, edge):
    return edge.replace(src=src, dst=dst)


def _replace(gtype):
    if isinstance(gtype, DiGraph):
        return _replace_diedge

    elif isinstance(gtype, Graph):
        return _replace_edge

    else:
        raise AttributeError


def _bfs_nodes(g, source):
    nodes = g.nodes
    queue = deque()
    visited = set()

    # initialize queue
    queue.append(iter([source, ]))

    # while queue is not empty
    while queue:
        # peek front of queue
        keys = queue[0]

        try:
            # next key
            key = next(keys)

            # if neighbor not already visited
            if key not in visited:
                nbr = nodes[key]

                # yield neighbor node
                yield nbr

                # push neighbors onto queue
                queue.append(iter(adjacency(nbr).keys))
                visited.add(key)

        except StopIteration:
            queue.popleft()


def _bfs_edges(g, source):
    nodes = g.nodes
    queue = deque()
    visited = set()
    replace = _replace(g)

    # initialize queue
    src = nodes[source]
    queue.append((src, iter(adjacency(src).pairs)))
    visited.add(source)

    while queue:
        # peek front of queue
        src, pairs = queue[0]

        try:
            # next key -> edge pair
            key, edge = next(pairs)

            # if neighbor not already visited
            if key not in visited:
                dst = nodes[key]

                # yield edge
                yield replace(src, dst, edge)

                # push neighbors onto queue
                queue.append((dst, iter(adjacency(dst).pairs)))
                visited.add(key)

        except StopIteration:
            queue.popleft()


def _bfs_paths(g, source):
    nodes = g.nodes
    queue = deque()
    visited = set()

    # initialize queue
    src = nodes[source]
    queue.append(((src,), iter(adjacency(src).keys)))
    visited.add(source)

    # while queue is not empty
    while queue:
        # peek front of queue
        path, keys = queue[0]

        try:
            # next key
            key = next(keys)

            # if neighbor not already visited
            if key not in visited:
                nbr = nodes[key]
                path += (nbr,)

                # yield path
                yield path

                # push neighbors onto queue
                queue.append((path, iter(adjacency(nbr).keys)))
                visited.add(key)

        except StopIteration:
            queue.popleft()


def _dfs_nodes(g, source):
    nodes = g.nodes
    stack = deque()
    visited = set()

    # initialize stack
    stack.append(iter([source, ]))

    # while stack is not empty
    while stack:
        # peek top of stack
        keys = stack[-1]

        try:
            # next key
            key = next(keys)

            # if neighbor not already visited
            if key not in visited:
                nbr = nodes[key]

                # yield neighbor node
                yield nbr

                # push neighbors onto stack
                stack.append(iter(adjacency(nbr).keys))
                visited.add(key)

        except StopIteration:
            stack.pop()


def _dfs_edges(g, source):
    nodes = g.nodes
    stack = deque()
    visited = set()

    # initialize stack
    src = nodes[source]
    stack.append(iter(adjacency(src).pairs))
    visited.add(source)

    while stack:
        pairs = stack[-1]

        try:
            key, edge = next(pairs)
            if key not in visited:
                nbr = nodes[key]
                stack.append(iter(adjacency(nbr).pairs))
                visited.add(key)
                yield edge

        except StopIteration:
            stack.pop()


def _dfs_paths(g, source):
    nodes = g.nodes
    stack = deque()
    visited = set()

    src = nodes[source]
    stack.append((iter(adjacency(src).keys), (src,)))
    visited.add(source)

    while stack:
        keys, path = stack[-1]

        try:
            key = next(keys)
            if key not in visited:
                nbr = nodes[key]
                path += (nbr, )
                stack.append((iter(adjacency(nbr).keys), path))
                visited.add(key)
                yield path

        except StopIteration:
            stack.pop()


def bfs(g, source, mode='nodes'):
    if mode == 'nodes':
        return _bfs_nodes(g, source)

    elif mode == 'edges':
        return _bfs_edges(g, source)

    elif mode == 'paths':
        return _bfs_paths(g, source)

    else:
        raise AttributeError


def dfs(g, source, mode='nodes'):
    if mode == 'nodes':
        return _dfs_nodes(g, source)

    elif mode == 'edges':
        return _dfs_edges(g, source)

    elif mode == 'paths':
        return _dfs_paths(g, source)

    else:
        raise AttributeError

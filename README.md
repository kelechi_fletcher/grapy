# GraPy #

Fast, memory-efficient graph data structures for Python!

## Tutorial ##

### Constructing a graph ##

Constructing an empty graph:
```python
import grapy as gp
g = gp.Graph()
```

Constructing a simple graph from node and edge records:
```python
import grapy as gp
nodes = [(0,), (1,), (2,)]
edges = [(0, 1), (1, 2), (2, 0)]
g = gp.Graph(nodes=(nodes, gp.Node), edges=(edges, gp.Edge))
```

Constructing a simple graph from just edge records:
```python
import grapy as gp
edges = [(0, 1), (1, 2), (2, 0)]
g = gp.Graph.from_edges(edges, gp.Edge)
```
